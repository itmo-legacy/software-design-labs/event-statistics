module Main where

import Control.Monad (replicateM_)

import StatisticsL (Event(..), StatisticsL(..))
import qualified StatisticsL.IO as Stats (doIO, runInterpretSession)

main :: IO ()
main = Stats.runInterpretSession session
  where
    session = do
      registerEvent (Event "one")
      registerEvent (Event "two")
      Stats.doIO (putStrLn "registering events")
      replicateM_ 4 (registerEvent (Event "one"))
      replicateM_ 3 (registerEvent (Event "two"))
      oneRPM <- getEventRPM (Event "one")
      Stats.doIO (print oneRPM)
      printTotalRPM
      registerEvent (Event "two")
      registerEvent (Event "two")
      registerEvent (Event "one")
      twoRPM <- getEventRPM (Event "two")
      Stats.doIO (print twoRPM)
      printTotalRPM
