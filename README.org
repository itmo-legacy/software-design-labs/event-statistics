You can find example usage of the language in [[file:app/Main.hs]]. Run it
with =stack run=.

Tests are in [[file:test/test.hs]], run them with =stack test=.

We give a general interface for the statistics language in
[[file:src/StatisticsL.hs]]. In [[file:src/StatisticsL/StatsOwner.hs]] we
derive an implementation for the interface for any type that can give
us current moment (~ClockL m~), can print ~Text~ (~IsPrinter m~) and
can manipulate ~Stats~ (~MonadState Stats m~). The derivation is done
via ~StatsOwner~.

In [[file:src/StatisticsL/IO.hs]] we give a concrete implementation for
~StatisticsL~ for IO via ~StatsOwner~.

In [[file:test/StatisticsL/Test.hs]] we make another concrete
implementation, pure and inspectable with a fast-forwardable clock.
