module IsPrinter
  ( IsPrinter(..)
  ) where

import Data.Text (Text)

class IsPrinter m where
  putText :: Text -> m ()
  putTextLn :: Text -> m ()
  putTextLn text = putText (text <> "\n")
