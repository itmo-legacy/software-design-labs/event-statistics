module StatisticsL
  ( StatisticsL (..)
  , RequestsPerMinute (..)
  , Event (..)
  ) where

import Data.Hashable (Hashable)
import Data.Text (Text)

newtype Event = Event Text
  deriving newtype (Eq, Hashable)
  deriving stock Show

newtype RequestsPerMinute = RPM Double
  deriving stock (Eq, Show)

class StatisticsL l where
  registerEvent :: Event -> l ()
  getEventRPM :: Event -> l RequestsPerMinute
  getTotalRPM :: l RequestsPerMinute
  printTotalRPM :: l ()
