module StatisticsL.IO
  ( Interpret (..)
  , runInterpretSession
  , doIO
  ) where

import Control.Monad.Reader (ReaderT(..), ask, lift, runReaderT)
import Control.Monad.State (MonadState(..))
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import qualified Data.Text.IO as Text (putStr, putStrLn)
import System.Clock (Clock(Monotonic), getTime)

import ClockL (ClockL(..))
import IsPrinter (IsPrinter(..))
import StatisticsL (StatisticsL(..))
import StatisticsL.StatsOwner (Stats, StatsOwner(..))

runInterpretSession :: Interpret a -> IO a
runInterpretSession action = do
  ref <- newIORef mempty
  runReaderT (runInterpret action) ref

newtype Interpret a = Interpret{ runInterpret :: ReaderT (IORef Stats) IO a }
  deriving newtype (Functor, Applicative, Monad)
  deriving StatisticsL via (StatsOwner Interpret)

instance ClockL Interpret where
  getCurrentMoment = doIO (getTime Monotonic)

instance IsPrinter Interpret where
  putText = doIO . Text.putStr
  putTextLn = doIO . Text.putStrLn

instance MonadState Stats Interpret where
  get = Interpret (ask >>= \ref -> lift (readIORef ref))
  put s = Interpret (ask >>= \ref -> lift (writeIORef ref s))

doIO :: IO a -> Interpret a
doIO = Interpret . lift
