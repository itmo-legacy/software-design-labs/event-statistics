{-# LANGUAGE UndecidableInstances #-}

module StatisticsL.StatsOwner
  ( StatsOwner(..)
  , Stats
  ) where

import Control.Monad.State (MonadState(..), modify')
import Data.HashMap.Lazy (HashMap)
import qualified Data.HashMap.Lazy as Map (alter, lookup)
import Data.MultiSet (MultiSet)
import qualified Data.MultiSet as Set (insert, singleton, size)
import qualified Data.Text as Text (pack)
import System.Clock (TimeSpec)
import Text.Printf (printf)

import ClockL (ClockL(..), takeLastHour, takeLastHour')
import IsPrinter (IsPrinter(..))
import StatisticsL (Event, RequestsPerMinute(RPM), StatisticsL(..))

type TimeLine = MultiSet TimeSpec

type Stats = HashMap Event TimeLine

newtype StatsOwner m a = StatsOwner{ unStatsOwner :: m a }

-- | If a number of requests for an event is lower than this number,
-- don't try to trim it to the last hour.
trimmingTriggerThreshold :: Int
trimmingTriggerThreshold = 10^(3 :: Int)

instance (ClockL m, IsPrinter m, MonadState Stats m)
  => StatisticsL (StatsOwner m) where

  registerEvent event = StatsOwner $ do
    now <- getCurrentMoment
    modify' (Map.alter (alterTimeline now) event)
    where
      alterTimeline now Nothing = Just (Set.singleton now)
      alterTimeline now (Just timeline) = Just (Set.insert now trimmedTimeline)
        where
          trimmedTimeline
            | Set.size timeline > trimmingTriggerThreshold
            = takeLastHour' timeline now
            | otherwise = timeline

  getEventRPM event = StatsOwner $ do
    stats <- get
    case Map.lookup event stats of
      Nothing -> pure (RPM 0)
      Just times -> do
        nRequests <- fmap Set.size (takeLastHour times)
        pure (RPM (fromIntegral nRequests / 60))

  getTotalRPM = StatsOwner $ do
    stats <- get
    lastHourStats <- traverse takeLastHour stats
    let nRequests = sum (fmap Set.size lastHourStats)
    pure (RPM (fromIntegral nRequests / 60))

  printTotalRPM = StatsOwner $ do
    RPM rpm <- unStatsOwner getTotalRPM
    putTextLn (Text.pack (printf "%.2f requests are made per minute" rpm))
