module ClockL
  ( ClockL (..)
  , takeLastHour
  , takeLastHour'
  , anHour
  , aMinute
  , aSecond
  , fromSecs
  , fromMins
  , fromHours
  ) where

import Data.MultiSet (MultiSet)
import qualified Data.MultiSet as Set (split)
import System.Clock (TimeSpec(..))

class ClockL l where
  getCurrentMoment :: l TimeSpec

takeLastHour :: (Monad l, ClockL l) => MultiSet TimeSpec -> l (MultiSet TimeSpec)
takeLastHour times = fmap (takeLastHour' times) getCurrentMoment

takeLastHour' :: MultiSet TimeSpec -> TimeSpec -> MultiSet TimeSpec
takeLastHour' times now
  | now < anHour = times
  | otherwise = snd (Set.split hourAgo times)
  where
    hourAgo = now - anHour

anHour :: TimeSpec
anHour = fromHours 1

aMinute :: TimeSpec
aMinute = fromMins 1

aSecond :: TimeSpec
aSecond = fromSecs 1

fromSecs :: Int -> TimeSpec
fromSecs n = TimeSpec{ sec = fromIntegral n, nsec = 0 }

fromMins :: Int -> TimeSpec
fromMins n = fromSecs (60 * n)

fromHours :: Int -> TimeSpec
fromHours n = fromMins (n * 60)
