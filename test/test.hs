import Control.Monad (replicateM_)
import qualified Data.MultiSet as Set (fromList, union)
import qualified Data.Text as Text (lines)
import Test.Tasty (TestTree, defaultMain, testGroup)
import Test.Tasty.HUnit (testCase)

import ClockL (aMinute, anHour, fromMins, takeLastHour')
import StatisticsL (Event(..), RequestsPerMinute(..), StatisticsL(..))
import StatisticsL.Test (fastForward)
import qualified StatisticsL.Test as Stat (runInterpretSession)

import FixedExpectations (shouldBe)

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "/"
  [ groupTakeLastHour'
  , groupStatisticsL
  ]

groupTakeLastHour' :: TestTree
groupTakeLastHour' = testGroup "takeLastHour'"
  [ testCase "simultaneous" testSimultaneous
  , testCase "several events some older than an hour" testHourApart
  ]
  where
    testSimultaneous = do
      let timeline = Set.fromList [0, 0, 0]
      takeLastHour' timeline 0 `shouldBe` timeline

    testHourApart = do
      let old = Set.fromList [0, 1, 5, 8]
      let new = Set.fromList [anHour + t | t <- [0, 3, 6, 9]]
      let whole = Set.union old new
      takeLastHour' whole (anHour + 10) `shouldBe` new

groupStatisticsL :: TestTree
groupStatisticsL = testGroup "StatisticsL"
  [ testCase "one event, registered several times in one moment"
    testSimultaneousOneEvent
  , testCase "several events, registered several times in one moment"
    testSimultaneousSeveralEvents
  , testCase "several events, hour apart" testHourApartEvents
  ]
  where
    testSimultaneousOneEvent = do
      let ((oneRPM, totalRPM), response) = Stat.runInterpretSession session
      oneRPM `shouldBe` RPM (3 / 60)
      totalRPM `shouldBe` RPM (3 / 60)
      Text.lines response `shouldBe`
        [ "0.05 requests are made per minute" ]
      where
        session = do
          replicateM_ 3 (registerEvent (Event "one"))
          oneRPM <- getEventRPM (Event "one")
          totalRPM <- getTotalRPM
          printTotalRPM
          pure (oneRPM, totalRPM)

    testSimultaneousSeveralEvents = do
      let ((oneRPM, oneRPM', twoRPM, totalRPM), response) =
            Stat.runInterpretSession session
      oneRPM `shouldBe` RPM 0
      oneRPM' `shouldBe` RPM (3 / 60)
      twoRPM `shouldBe` RPM (4 / 60)
      totalRPM `shouldBe` RPM (7 / 60)
      Text.lines response `shouldBe`
        [ "0.12 requests are made per minute" ]
      where
        session = do
          oneRPM <- getEventRPM (Event "one")
          replicateM_ 3 (registerEvent (Event "one"))
          replicateM_ 4 (registerEvent (Event "two"))
          oneRPM' <- getEventRPM (Event "one")
          twoRPM <- getEventRPM (Event "two")
          totalRPM <- getTotalRPM
          printTotalRPM
          pure (oneRPM, oneRPM', twoRPM, totalRPM)

    testHourApartEvents = do
      let
        (( oneRPM0, oneRPM1
         , twoRPM0, twoRPM1
         , totalRPM0, totalRPM1
         ), response) =
            Stat.runInterpretSession session
      oneRPM0 `shouldBe` RPM (5 / 60)
      oneRPM1 `shouldBe` RPM (1 / 60)
      twoRPM0 `shouldBe` RPM (4 / 60)
      twoRPM1 `shouldBe` RPM (2 / 60)
      totalRPM0 `shouldBe` RPM (9 / 60)
      totalRPM1 `shouldBe` RPM (3 / 60)
      Text.lines response `shouldBe`
        [ "0.15 requests are made per minute"
        , "0.05 requests are made per minute"
        ]
      where
        session = do
          registerEvent (Event "one")
          registerEvent (Event "two")
          fastForward aMinute
          replicateM_ 4 (registerEvent (Event "one"))
          fastForward (fromMins 6)
          replicateM_ 3 (registerEvent (Event "two"))
          oneRPM0 <- getEventRPM (Event "one")
          twoRPM0 <- getEventRPM (Event "two")
          totalRPM0 <- getTotalRPM
          printTotalRPM
          fastForward anHour
          registerEvent (Event "two")
          fastForward aMinute
          registerEvent (Event "two")
          registerEvent (Event "one")
          oneRPM1 <- getEventRPM (Event "one")
          twoRPM1 <- getEventRPM (Event "two")
          totalRPM1 <- getTotalRPM
          printTotalRPM
          pure ( oneRPM0, oneRPM1
               , twoRPM0, twoRPM1
               , totalRPM0, totalRPM1
               )
