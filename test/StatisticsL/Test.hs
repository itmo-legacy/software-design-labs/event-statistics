{-# LANGUAGE TemplateHaskell #-}

module StatisticsL.Test
  ( Interpret (..)
  , eStats
  , eNow
  , runInterpretSession
  , fastForward
  ) where

import Control.Arrow (second)
import Control.Monad.State (MonadState(..), State, StateT(..), evalState, gets, modify')
import Control.Monad.Writer (WriterT(..), tell)
import Data.Function ((&))
import Data.Text (Text)
import Lens.Micro.Platform (makeLenses, over)
import System.Clock (TimeSpec)

import ClockL (ClockL(..))
import IsPrinter (IsPrinter(..))
import StatisticsL (StatisticsL)
import StatisticsL.StatsOwner (Stats, StatsOwner(..))

data Env = Env
  { _eStats :: Stats
  , _eNow :: TimeSpec
  }

$(makeLenses ''Env)

emptyEnv :: Env
emptyEnv = Env{ _eStats = mempty, _eNow = 0 }

runInterpretSession :: Interpret a -> (a, Text)
runInterpretSession s
  = runInterpret s
  & runWriterT
  & flip evalState emptyEnv

newtype Interpret a = Interpret
  { runInterpret :: WriterT Text (State Env) a
  }
  deriving newtype (Functor, Applicative, Monad)
  deriving StatisticsL via (StatsOwner Interpret)

instance ClockL Interpret where
  getCurrentMoment = Interpret (gets _eNow)

instance MonadState Stats Interpret where
  state action = Interpret $
    state (\env -> action (_eStats env) & second (setStats env))
    where
      setStats env newStats = env{ _eStats = newStats }

instance IsPrinter Interpret where
  putText = Interpret . tell

fastForward :: TimeSpec -> Interpret ()
fastForward time = Interpret (modify' (over eNow (+ time)))
